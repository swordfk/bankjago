package com.example.myapplication5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class PembayaranAct extends AppCompatActivity {
    boolean terisi = false;
    boolean ceklist1 = false;
    boolean ceklist2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RelativeLayout layout1 = findViewById(R.id.layout1);
        RelativeLayout layout2 = findViewById(R.id.layout2);
        ImageView cek1 = findViewById(R.id.ceklist1);
        ImageView cek2 = findViewById(R.id.ceklist2);
        Button bayar = findViewById(R.id.btn_bayar);

        if(!ceklist1){
            cek1.setVisibility(View.GONE);
        }
        if(!ceklist2){
            cek2.setVisibility(View.GONE);
        }

        if (!terisi){
            bayar.setEnabled(false);
            bayar.setBackgroundColor(0xFFFEE9A8);
            bayar.setTextColor(0xFF989B9D);
        }

        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ceklist1){
                    cek1.setVisibility(View.GONE);
                    terisi = false;
                    ceklist1 = false;
                    bayar.setEnabled(false);
                    bayar.setBackgroundColor(0xFFFEE9A8);
                    bayar.setTextColor(0xFF989B9D);
                }else {
                    ceklist1 = true;
                    cek1.setVisibility(View.VISIBLE);
                    cek2.setVisibility(View.GONE);
                    terisi = true;
                    bayar.setEnabled(true);
                    bayar.setBackgroundColor(0xFFFDAF27);
                    bayar.setTextColor(0xFF000000);
                }
            }
        });

        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ceklist2){
                    cek2.setVisibility(View.GONE);
                    terisi = false;
                    ceklist2 = false;
                    bayar.setEnabled(false);
                    bayar.setBackgroundColor(0xFFFEE9A8);
                    bayar.setTextColor(0xFF989B9D);
                }else {
                    ceklist2 = true;
                    cek2.setVisibility(View.VISIBLE);
                    cek1.setVisibility(View.GONE);
                    terisi = true;
                    bayar.setEnabled(true);
                    bayar.setBackgroundColor(0xFFFDAF27);
                    bayar.setTextColor(0xFF000000);
                }
            }
        });

        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PembayaranBerhasil.class));
            }
        });
    }
}